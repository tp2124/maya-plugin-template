# maya-plugin-template
This was initially tested with Maya 2017

This repository is meant to help with a starting place for the solution and project setup that are required to make a binary that is able to host code for Maya plugins with functions that will be accessible via scripting interfaces.

Work based on tutorial from http://www.chadvernon.com/blog/resources/maya-api-programming/your-first-plug-in/